var me = {
    nome: "Fausto",
    cognome: "Sgobba",
    eta: 43,
    "sport preferiti": [
        "Nuoto",
        "Snowboard"
    ]
};
console.log(me);
console.log("nome:"+me.nome);
console.log("nome:"+me["nome"]);
console.log("sport preferiti:"+me["sport preferiti"]);
me.sesso = "Maschile";

if (me.eta>45) {
    console.log(me.nome+" ha più di 45 anni");
} else {
    console.log(me.nome+" ha meno di 45 anni");
}

me.eta = 49;
console.log(me);
console.log(typeof me);

if (me.eta>45) {
    console.log(me.nome+" ha più di 45 anni");
} else {
    console.log(me.nome+" ha meno di 45 anni");
}

if (me.sesso!=="Maschile") console.log(me.nome+" non è un uomo");
if (me.sesso==="Maschile") console.log(me.nome+" è un uomo");

if (typeof me2 !== "undefined") {
    console.log("me2 è definita:",me2);
}

if (typeof me !== "undefined") {
    console.log("me è definita:",me);
}