var colori;
colori = ["rosso","giallo","verde"];
console.log("posizione 0="+colori[0]);
console.log("posizione 1="+colori[1]);
console.log("posizione 2="+colori[2]);
console.log("posizione 3="+colori[3]);
console.log("array completa:");
console.log(colori);
console.log("colori:"+colori);


var colori2 = colori[0] + " chiaro";

console.log("array completa:");
console.log(colori);
console.log("colori2="+colori2);

colori[3] = "blu";
console.log("array completa + 1 valore:");
console.log(colori);

colori.push("marrone");
console.log("array completa dopo il push:");
console.log(colori);

colori.push(150);
console.log("array completa dopo il push:");
console.log(colori);

console.log("tipo valore 0:"+typeof colori[0]);
console.log("tipo valore 5:"+typeof colori[5]);
console.log("tipo array completa:"+typeof colori);

colori.push([1,2,3,4,5]);
console.log("nested array:");
console.log(colori);
console.log("terzo valore nested array:"+colori[6][2]);
console.log(
    "è una stringa:"
    +(typeof colori[6][2] === "string")
    );
console.log(
    "è un numero:"
    +(typeof colori[6][2] === "number")
    );