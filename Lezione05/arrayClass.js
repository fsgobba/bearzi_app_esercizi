var arr = [1,2,3,4,5,6];
console.log(arr);
var removed_item = arr.pop();
console.log(removed_item);
console.log(arr);
var reversed_array = arr.reverse();
console.log(arr);
console.log(reversed_array);
var shifted_var = arr.shift();
console.log(shifted_var);
console.log(arr);
// voglio aggiungere 5 e 6 dopo il 3
arr.splice(2,0,5,6);
console.log(arr);
// voglio togliere il secondo valore
var removed_item = arr.splice(1,1);
console.log(removed_item);
console.log(arr);
arr.unshift(11,12);
console.log(arr);
var joined = arr.join("-");
console.log(joined);
var frase = "Frase un po lunga da invertire con javascript";
var invertita = frase.split("").reverse().join("");
console.log(frase);
console.log(invertita);
var arr1 = ["Rosso", "Verde", "Giallo", "Nero"];
var arr2 = arr1.join("/").split("/");
// questo genera solo un puntatore, quindi è la stessa array con due nomi diversi
//var arr2 = arr1; 
arr2.reverse();
console.log(arr1);
console.log(arr2);
console.log(frase.charAt(1));
console.log(frase.charAt(frase.length-1));
console.log(frase.indexOf("javascript"));
console.log(frase.charAt(35));
console.log(frase.indexOf("Javascript"));
var frase2 = frase;
var changed;
while ((typeof changed == "undefined") || (changed)) {
    var temp = frase2.replace("a", "?");
    if (temp==frase2) {
        changed = false;
    } else {
        frase2 = temp;
    }
    console.log(frase2);
}
console.log(frase);
console.log(frase2);