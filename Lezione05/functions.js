function sum(a,b,callback) {
    var risultato = a+b;
    if (typeof callback == "function") callback(risultato);
    return risultato;
}

function quad(valore) {
    console.log("Il quadrato di "+valore+" è "+(valore*valore));
}

function double(valore) {
    console.log("Il doppio di valore è "+(valore*2));
}

console.log(sum(5,6,quad));
console.log(sum(5,6,double));
console.log(sum(15,46));
console.log(sum(45,64));

var stringa = "Buongiorno a tutti";
console.log(stringa.split("a"));
console.log(stringa.split(" "));
//console.log(stringa.split(""));
var numero = 54895574;
numero = numero.toString();
console.log(numero.split(""));
var num2 = "456789";
console.log(typeof num2);
console.log(typeof parseInt(num2));

var nome = "fausto";
var nomeMaiuiscolo = nome.toUpperCase();
console.log(nomeMaiuiscolo);
var nomeMinuscolo = nomeMaiuiscolo.toLowerCase();
console.log(nomeMinuscolo);

