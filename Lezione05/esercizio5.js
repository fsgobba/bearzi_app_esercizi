function contaLettere(frase) {
    // trasformo la frase in minuscolo
    frase = frase.toLowerCase();
    // trasformo la frase in un'array di caratteri singoli
    var arr_caratteri = frase.split("");
    // creo un hash dove salvare il contatore di ogni lettera trovata
    var risultato = {};
    // definisco l'array dei caratteri ammessi
    var ammessi = ("abcdefghilmnopqrstuyxwvzàèùìòjk").split("");
    // faccio un loop su tutte le lettere
    for (var i=0;i<arr_caratteri.length;i++) {
        // salvo in una variabile temporanea la lettera corrente
        var lettera = arr_caratteri[i];
        if (ammessi.indexOf(lettera)>=0) {
            // aumento il contatore della lettera trovata
            if (risultato[lettera]) {
                risultato[lettera] += 1;
            } else {
                risultato[lettera] = 1;
            }
        }       
    }
    
    // restituisco il risultato
    return risultato;
}
function stampaInConsole(oggetto) {
    for (var i in oggetto) {
        var contatore = oggetto[i];
        console.log("La lettera "+i+" compare "+contatore+" volte nella frase");
    }
}
function stampaHTML(oggetto) {
    var temp_arr = [];
    for (var i in oggetto) {
        var contatore = oggetto[i];
        temp_arr.push("La lettera "+i+" compare "+contatore+" volte nella frase");
    }
    document.write("<p>"+temp_arr.join("</p><p>")+"</p>");
}
// definisco la frase da controllare
var frase = "Buongiorno, questa è la sesta lezione al bearzi";
// richiamo la funzione
var risultato = contaLettere(frase);
stampaInConsole(risultato);
stampaHTML(risultato);